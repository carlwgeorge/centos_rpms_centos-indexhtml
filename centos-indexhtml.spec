Summary: Browser default start page for CentOS
Name: centos-indexhtml
Version: 9.0
Release: 0%{?dist}
Source: %{name}-%{version}.tar.gz
License: Distributable
Group: Documentation
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: indexhtml <= 2:5-1
Obsoletes: redhat-indexhtml
Provides: redhat-indexhtml

%description
The indexhtml package contains the welcome page shown by your Web browser,
which you'll see after you've successfully installed CentOS Linux.

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_defaultdocdir}/HTML
cp -a . $RPM_BUILD_ROOT/%{_defaultdocdir}/HTML/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_defaultdocdir}/HTML/*

%changelog
* Wed Mar 3 2021 Michal Konecny <mkonecny@redhat.com> 9.0-0
- Update for CentOS 9 Stream

* Sun Apr 28 2019 Alain Reguera Delgado <alain.reguera@gmail.com> 8.0-0
- Roll in CentOS artwork based on CentOS Artistic Motif Sketch 4

* Sun Jun 29 2014 Johnny Hughes <johnny@centos.org> 7-9.el7.centos
- Add en-US directory

* Fri May 16 2014 Johnny Hughes <johnny@centos.org> 7-8.el7.centos
- Roll in CentOS Branding
